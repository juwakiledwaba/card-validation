import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  input1: number;
  visa: boolean;
  mastercard: boolean;


  detectChange(event){
    console.log(event)

    if(event === '4'){ 
      console.log('visa')
      this.visa = true;
      this.mastercard = false;

    } else if(event === '5'){
      console.log('mastercard')

      this.visa = false;
      this.mastercard = true;
    }
  
  }

}
